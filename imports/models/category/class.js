import { Class } from 'meteor/jagi:astronomy';//importar libreria
const categoryCollection = new Mongo.Collection('category',{idGeneration:'MONGO'});//crear coleccion o tabla en mongo y idGeneration:'MONGO' par que sea un objet id

export const categoryClass = Class.create({
  name: 'categoryClass ',
  collection: categoryCollection,
  fields: {
    name: {
      type: String,
      validators: [{
        type: 'regexp',
        param: /^[a-zA-Z0-9\s\u00f1\u00d1]{6,}$/g,
        resolveError({ name, param }) {
          return `el nombre de la categoria debe tener solo letras y debe ser mayor igual a6 letras`;
        }
      }]
    },
    description:{
        type: String,
    },
    active:{
      type:Boolean,
      default:true
    }
   
  }
});
if (Meteor.isServer) {
    categoryClass.extend({
      fields: {
        user: String,
        
      },
      behaviors: {
        timestamp: {
          hasCreatedField: true,
          createdFieldName: 'createdAt',
          hasUpdatedField: true,
          updatedFieldName: 'updatedAt'
        }
      }
    });
  }
  