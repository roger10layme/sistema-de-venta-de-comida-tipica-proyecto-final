Meteor.methods({
    userCreate:function(form){
        //console.log(form)
        ///return 'hola mundo desde el servidor '
        if(form.password != form.re_password)
            throw new Meteor.Error(406, "la contraseña debe de ser la misma que la de la confirmacion");
        const _id = Accounts.createUser({
            createdAt:new Date(),
            username:form.username,
            email:form.email,
            password:form.password,
            profile:{
                fullname:form.nameComplete
            }
        })

        const roles =Meteor.roles.find({group:'people'}).fetch()
        const rolesuser = []
        roles.forEach(element => {
            rolesuser.push(element._id)
        });
        Roles.addUsersToRoles(_id,rolesuser)
        return 'Usuario creado correctamente'
    }
})
Meteor.methods({
    createRoles(){
        /*Meteor.roles.insert({
            _id:'createpublication',
            description:'crear publicacion',
            groupdescription:'publicaciones',
            group:'people',
            children:[],
            key:new Meteor.Collection.ObjectID()
        })
        Meteor.roles.insert({_id:'allpublicationuser',description:'ver mis publicaciones',groupdescription:'publicaciones',group:'people',children:[],key:new Meteor.Collection.ObjectID()})
        Meteor.roles.insert({_id:'editpublication',description:'editar publicacion',groupdescription:'publicaciones',group:'people',children:[],key:new Meteor.Collection.ObjectID()})
        Meteor.roles.insert({_id:'downpublication',description:'eliminar publicacion ',groupdescription:'publicaciones',group:'people',children:[],key:new Meteor.Collection.ObjectID()})
        Meteor.roles.insert({_id:'createCategory',description:'crear categoria',groupdescription:'categorias',group:'admin',children:[],key:new Meteor.Collection.ObjectID()})
        Meteor.roles.insert({_id:'viewallCategory',description:'ver categorias',groupdescription:'categorias',group:'admin',children:[],key:new Meteor.Collection.ObjectID()})*/
        
        Roles.setUserRoles(Meteor.userId(),["createpublication","allpublicationuser","editpublication","downpublication","createCategory","viewallCategory"])
        return 'guardado correectamente'

        
    }
})
Meteor.publish(null, function () {
    if (this.userId) {
      return Meteor.roleAssignment.find({ 'user._id': this.userId });
    } else {
      this.ready()
    }
  })
