import { Class } from 'meteor/jagi:astronomy';//importar libreria
const publicationsCollection = new Mongo.Collection('publications',{idGeneration:'MONGO'});//crear coleccion o tabla en mongo y idGeneration:'MONGO' par que sea un objet id

export const publicationsClass = Class.create({
  name: 'publicationsClass ',
  collection: publicationsCollection,
  fields: {
    title: {
      type: String,
      //default:'hola mundo ',//valor por defecto
      //optional:true //condicional de el campo no es obligatorio llenar
    },
    description:{
        type: String,
    },
    source:{
      type: String,
  },
    phone:{
        type: String,
    },
    price:{
        type: Number,
    },
    starDate:{
        type: Date,
    },
    endDate:{
        type: Date,
    },
    nameCategory:{
      type:String,
    },
    idfile :{
      type:String,
    },
    urlfile:{
      type:String,
    },
    active:{
      type:Boolean,
      default:true,
    },
    idCategory:{
      type:Mongo.ObjectID,
    },
    typepub:{
      type:String,
      default:'image'
    },   
    username:String,
    created_view:{
      type:Date,
    }



  }
});

if (Meteor.isServer) {
  publicationsClass .extend({
    fields: {
      user: String,
      

    },
    behaviors: {
      timestamp: {
        hasCreatedField: true,
        createdFieldName: 'createdAt',
        hasUpdatedField: true,
        updatedFieldName: 'updatedAt'
      }
    }
  });
}