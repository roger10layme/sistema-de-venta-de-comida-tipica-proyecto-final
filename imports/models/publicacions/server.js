import {publicationsClass} from './class'
import {categoryClass} from '../category/class'
import UserFiles from '../files/class'
import { Link } from 'react-router-dom'
import {queryPublications} from './querys'

publicationsClass.extend({
    meteorMethods:{
        newPublication(form){
            try {
                if(Roles.userIsInRole(Meteor.userId(),"createpublication")){
                    console.log(form)
                    const file= UserFiles.findOne({_id:form.image})
                    const URL = UserFiles.findOne({_id:form.image}).link()
                    const indextext=file.mime.lastIndexOf('/')
                    var newUrl = URL.replace(/^.*\/\/[^\/]+/,'')
                    const category = categoryClass.findOne({_id:new Meteor.Collection.ObjectID(form.category)})
                    this.title=form.title
                    this.description=form.description
                    this.source=form.source
                    this.phone=form.phone
                    this. price=parseInt (form.price)
                    this.nameCategory = category.name
                    this.idCategory = category._id
                    this.idfile = form.image
                    this.urlfile = newUrl
                    this.starDate=form.starDate
                    this.endDate=form.endDate
                    this.user=Meteor.userId()
                    this.username=Meteor.user().username
                    this.created_view= new Date()
                    this.typepub=file.mime.slice(0,indextext)
                    this.save()
                    return 'Guardado correctamente'
                }else{
                    throw new Meteor.Error(403,'ACCESO DENEGADO')
                }
                
            } catch (error) {
                console.log(error)
                throw new Meteor.Error(403,error.reason)
                
            }
           
            //console.log(this)
            //metdo para remplazar la prpiedades o campos que llegan desde el cliente
           // this.set(form) //esto cambia los valores de las propiedades con los mismos nombre que llegan del cliente
            

            //metdo para remplazar la prpiedades o campos que llegan desde el cliente
            /*this.title=form.title
            this.description=form.description
            this.source=form.source
            this.phone=form.phone
            this. price=parseInt (form.precio)
            this.starDate=form.starDate
            this.endDate=form.endDate*/
            
            //this.save()
           
        },
        editPublication(form){
            try {
                if(form.image){
                    //console.log(form)
                    const file= UserFiles.findOne({_id:form.image})
                    const URL = file.link()
                    const indextext=file.mime.lastIndexOf('/')
                    var newUrl = URL.replace(/^.*\/\/[^\/]+/, '')
                    const category = categoryClass.findOne({ _id: new Meteor.Collection.ObjectID(form.category) })
                    this.title = form.title
                    this.description = form.description
                    this.source = form.source
                    this.phone = form.phone
                    this.price = parseInt(form.price)
                    this.nameCategory = category.name
                    this.idCategory = category._id
                    this.idfile = form.image
                    this.urlfile = newUrl
                    this.starDate = form.starDate
                    this.endDate = form.endDate
                    this.user = Meteor.userId()
                    this.username = Meteor.user().username
                    this.created_view = new Date()
                    this.typepub =file.mime.slice(0,indextext)
                    this.save()
                    return 'Editado correctamente con imagen'
                }else{
                    //console.log(form)
                   // const URL = UserFiles.findOne({ _id: form.image }).link()
                    //var newUrl = URL.replace(/^.*\/\/[^\/]+/, '')
                    const category = categoryClass.findOne({ _id: new Meteor.Collection.ObjectID(form.category) })
                    this.title = form.title
                    this.description = form.description
                    this.source = form.source
                    this.phone = form.phone
                    this.price = parseInt(form.price)
                    this.nameCategory = category.name
                    this.idCategory = category._id
                    //this.idfile = form.image
                    //this.urlfile = newUrl
                    this.starDate = form.starDate
                    this.endDate = form.endDate
                    this.user = Meteor.userId()
                    this.username = Meteor.user().username
                    this.created_view = new Date()
                    this.save()
                    return 'Editado correctamenten SIN imagen'
                }
                
            } catch (error) {
                console.log(error)
                throw new Meteor.Error(403,error.reason)
                
            }
           
        },
        updateStatePublication(){
            try{
                this.active=this.active?false:true
                return this.save()
            }catch(error){
                console.log(error)
                throw new Meteor.Error(403,error.reason)
            }
        }

    }
})

//aqui el cliente se susbribira para poder acceder a los datos de la base de datos
//Meteor.publish('mispublicaciones',function(){
    //return publicationsClass.find()//selecioname todo de la collecion publications
    //esto esta prohibido hacer 
//})
Meteor.publish('publications',function(options,type){
    try {
        const  subs= new queryPublications(options,this)
        return subs[type]()
    } catch (error) {
        this.stop()
        throw new Meteor.Error(403,error.reason)
    }
})