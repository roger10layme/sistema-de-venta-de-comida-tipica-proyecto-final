import React, { Component } from 'react'
import { withTracker } from 'meteor/react-meteor-data'
import {Switch, useParams} from 'react-router-dom'
import SwitchRoutes from '/imports/routes/SwitchRoutes'
import Nav from '/imports/componets/Nav'
import Sidebar from '/imports/componets/Sidebar'
import Home from '/imports/view/home/Home'
import { Meteor } from 'meteor/meteor';
import runGraphics from '/imports/assets/dashboard/js/page'
import "react-datepicker/dist/react-datepicker.css";
import {Roles} from 'meteor/alanning:roles'

const feather = require('feather-icons')

class Layout extends Component {
    constructor(props){
        super(props)
        this.state={
            username:null,
            loader:true
        }
    }
    componentDidMount(){
        import '/imports/assets/dashboard/css'
        import '/imports/assets/dashboard/js' 
        import dashboarDesign from '/imports/assets/dashboard/js/scripts'
        
        
        dashboarDesign()

    }
    componentDidUpdate(prevProps) {
        // Uso tipico (no olvides de comparar las props):
        if (this.props.username !== prevProps.username) {
          runGraphics()
          feather.replace();
          setTimeout(function(){
            $(".loader").fadeOut("slow");
          },1000)
        }
      }
    render(){
        const {routes,username} =this.props
        //console.log(username)
        return (
            <div>
                <div className="loader"></div>
                <div id="app">
                    <div className="main-wrapper main-wrapper-1">
                        <div className="navbar-bg"></div>
                        <Nav/>
                        <Sidebar/>
                        <div className="main-content">
                            <Switch>
                                {
                                routes.map((route,i)=>{
                                    if(route.permission){
                                       if(Roles.userIsInRole(Meteor.userId(),route.permission)){
                                            return <SwitchRoutes key={i} {...route} />
                                        }else{
                                            console.log('acceso denegado')
                                        }
                                        
                                    }else{
                                        return <SwitchRoutes key={i} {...route} />
                                    }
                                })
                                }

                            </Switch>
                         </div>
                        <footer className="main-footer">
                            <div className="footer-left">
                                <a href="templateshub.net">Templateshub</a>
                            </div>
                            <div className="footer-right">
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        )
    }
}

export default withTracker(params =>{
    return{
        username:Meteor.user()
    }
})(Layout)
 
