import React, {Component} from 'react'
import {publicationsClass} from '../../models/publicacions/class'
import {withTracker} from 'meteor/react-meteor-data'
import { categoryClass } from '../../models/category/class';
import { data } from 'jquery';
const categorySelect = new ReactiveVar(undefined) 

class Main extends Component {
    selectCategory=(category)=>{
        categorySelect.set(category?category:undefined)
    }
    render() {
        const {publications,suscriptionPublications,categorys,publicationsLimitTwo}=this.props
        
        return (
            <div>
                <section id="hero" className="d-flex align-items-center">
                    <div className="container position-relative text-center text-lg-start" data-aos="zoom-in"
                        data-aos-delay={100}>
                        <div className="row">
                            <div className="col-lg-8">
                                <h1>Bienvenido
                                    <span>Restaurante</span>
                                </h1>
                                <h2>De gusta de los platos tipicos de Bolivia</h2>
                                <div className="btns">
                                    <a href="#menu" className="btn-menu animated fadeInUp scrollto">Nuestro Menu</a>
                                    <a href="#book-a-table" className="btn-book animated fadeInUp scrollto">Reservar una mesa</a>
                                </div>
                            </div>
                            <div className="col-lg-4 d-flex align-items-center justify-content-center position-relative" data-aos="zoom-in"
                                data-aos-delay={200}>
                                <a href="https://www.youtube.com/watch?v=CZ6IUOPUrWg" className="glightbox play-btn"/>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="about" className="about">
                    <div className="container" data-aos="fade-up">
                        <div className="row">
                            <div className="col-lg-6 order-1 order-lg-2" data-aos="zoom-in"
                                data-aos-delay={100}>
                                <div className="about-img">
                                    <img src="/principal/img/salarP01.jpg" />
                                </div>
                            </div>
                            <div className="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 content">
                                <h3>Porque amamos la vida, le ponemos pasion ala comida</h3>
                                <p className="fst-italic">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                                                                        magna aliqua.
                                </p>
                                <ul>
                                    <li><i className="bi bi-check-circle"/>
                                        Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
                                    <li><i className="bi bi-check-circle"/>
                                        Duis aute irure dolor in reprehenderit in voluptate velit.</li>
                                    <li><i className="bi bi-check-circle"/>
                                        Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate trideta storacalaperda mastiro dolore eu fugiat nulla pariatur.</li>
                                </ul>
                                <p>
                                    Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                                    velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                                    sunt in  culpa qui officia deserunt mollit anim id est laborum
                                </p>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="blog_area section-padding">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-8 mb-5 mb-lg-0">
                                <div className="blog_left_sidebar">
                                    {
                                        publications.map((data, key) => {
                                            return <article key={`article_${key}`} className="blog_item">
                                                <div className="blog_item_img">
                                                    {data.typepub == 'video'?
                                                        (
                                                            <video className="card-img rounded-0" controls>
                                                                <source src={data.urlfile} />
                                                                
                                                            </video>
                                                        )
                                                        :
                                                        <img className="card-img rounded-0" src={data.urlfile} />
                                                    }
                                                    
                                                    <a href="#" className="blog_item_date">
                                                        <h3>{moment(data.created_view).format('DD')}</h3>
                                                        <p>{moment(data.created_view).format('MMM')}</p>
                                                    </a>
                                                </div>
                                                <div className="blog_details">
                                                    <a className="d-inline-block" href="blog_details.html">
                                                        <h2 className="blog-head" style={ {color: '#2d2d2d' } }>{data.title}</h2>
                                                    </a>
                                                    <p>{data.description}</p>
                                                    <ul className="blog-info-link">
                                                        <li>
                                                            <a href="#"><i className="fa fa-user" /> {data.username}</a>
                                                        </li>
                                                        <li>
                                                            <a href="#"><i className="fa fa-comments" />  03 Comments</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </article>
                                        })
                                    }
                                    <nav className="blog-pagination justify-content-center d-flex">
                                        <ul className="pagination">
                                            <li className="page-item">
                                                <a href="#" className="page-link" aria-label="Previous">
                                                    <i className="ti-angle-left"/>
                                                </a>
                                            </li>
                                            <li className="page-item">
                                                <a href="#" className="page-link">1</a>
                                            </li>
                                            <li className="page-item active">
                                                <a href="#" className="page-link">2</a>
                                            </li>
                                            <li className="page-item">
                                                <a href="#" className="page-link" aria-label="Next">
                                                    <i className="ti-angle-right"/>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div className="col-lg-4">
                                <div className="blog_right_sidebar">
                                    <aside className="single_sidebar_widget search_widget">
                                        <form action="#">
                                            <div className="form-group">
                                                <div className="input-group mb-3">
                                                    <input type="text" className="form-control" placeholder="Search Keyword"/>
                                                    <div className="input-group-append">
                                                        <button className="btns" type="button"><i className="ti-search"/></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <button className="button rounded-0 primary-bg text-white w-100 btn_1 boxed-btn" type="submit">Search</button>
                                        </form>
                                    </aside>
                                    <aside className="single_sidebar_widget post_category_widget">
                                        <h4 className="widget_title"
                                            style={ {color: '#cda45e'} }>Categorias</h4>
                                        <ul className="list cat-list">
                                                    <li >
                                                         <a style={{cursor:'pointer'}} className="d-flex" onClick={()=>{this.selectCategory(null)}}>
                                                             <p>Todas las Categorias</p>
                                                                        
                                                        </a>
                                                    </li>
                                            {
                                                categorys.map((data,key)=>{
                                                    return<li key={`article_${key}`}>
                                                        <a style={{cursor:'pointer'}}  className="d-flex" onClick={()=>{this.selectCategory(data)}}>
                                                            <p>{data.name}</p>
                                                            
                                                        </a>
                                                    </li>
                                                })
                                            }
                                        </ul>
                                    </aside>
                                    <aside className="single_sidebar_widget popular_post_widget">
                                        <h3 className="widget_title"
                                            style={ {color: '#cda45e'} }>Publicaciones recientes</h3>
                                        {
                                            publicationsLimitTwo.map((data,key)=>{
                                                return  <div  key={`article_${key}`} className="media post_item">
                                                            
                                                            {data.typepub == 'video'?
                                                                (
                                                                    <img src='/principal/img/video_img1.png' style={{width:'35%'}}/>
                                                                )
                                                                :
                                                                <img src={data.urlfile} style={{width:'35%'}}/>
                                                            }
                                                            <div className="media-body">
                                                                <a href="blog_details.html">
                                                                    <h3 style={{color: '#2d2d2d'}}>{data.title}</h3>
                                                                </a>
                                                                <p>{moment(data.created_view).format('MMMM DD, YYYY')}</p>
                                                            </div>
                                                        </div>
                                            })
                                        }
                                    </aside>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="why-us" className="why-us">
                    <div className="container" data-aos="fade-up">
                        <div className="section-title">
                            <h2>Por que Nosostros</h2>
                            <p>Por que Elegir Nuestro Restaurante</p>
                        </div>
                        <div className="row">
                            <div className="col-lg-4">
                                <div className="box" data-aos="zoom-in"
                                    data-aos-delay={100}>
                                    <span>01</span>
                                    <h4>Comida</h4>
                                    <p>Ulamco laboris nisi ut aliquip ex ea commodo consequat. Et consectetur ducimus vero placeat</p>
                                </div>
                            </div>
                            <div className="col-lg-4 mt-4 mt-lg-0">
                                <div className="box" data-aos="zoom-in"
                                    data-aos-delay={200}>
                                    <span>02</span>
                                    <h4>Jugos y Bebidas</h4>
                                    <p>Dolorem est fugiat occaecati voluptate velit esse. Dicta veritatis dolor quod et vel dire leno para dest</p>
                                </div>
                            </div>
                            <div className="col-lg-4 mt-4 mt-lg-0">
                                <div className="box" data-aos="zoom-in"
                                    data-aos-delay={300}>
                                    <span>03</span>
                                    <h4>
                                        Postres</h4>
                                    <p>Molestiae officiis omnis illo asperiores. Aut doloribus vitae sunt debitis quo vel nam quis</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="menu" className="menu section-bg">
                    <div className="container" data-aos="fade-up">
                        <div className="section-title">
                            <h2>Menu</h2>
                            <p>Consulta de Nuestro Sabroso Menu</p>
                        </div>
                        <div className="row" data-aos="fade-up"
                            data-aos-delay={100}>
                            <div className="col-lg-12 d-flex justify-content-center">
                                <ul id="menu-flters">
                                    <li data-filter="*" className="filter-active">All</li>
                                    <li data-filter=".filter-starters">Starters</li>
                                    <li data-filter=".filter-salads">Salads</li>
                                    <li data-filter=".filter-speciy">Speciy</li>
                                </ul>
                            </div>
                        </div>
                        <div className="row menu-container" data-aos="fade-up"
                            data-aos-delay={200}>
                            <div className="col-lg-6 menu-item filter-starters">
                                <img src="/principal/img/Menu/calapurca.jpg" className="menu-img" />
                                <div className="menu-content">
                                    <a href="#">Calapurca</a>
                                    <span>Bs 15.00</span>
                                </div>
                                <div className="menu-ingredients">
                                    Lorem, deren, trataro, filede, nerada
                                </div>
                            </div>
                            <div className="col-lg-6 menu-item filter-speciy">
                                <img src="/principal/img/Menu/charquekan.jpg" className="menu-img" />
                                <div className="menu-content">
                                    <a href="#">Charquekan</a>
                                    <span>Bs 20.95</span>
                                </div>
                                <div className="menu-ingredients">
                                    Lorem, deren, trataro, filede, nerada
                                </div>
                            </div>
                            <div className="col-lg-6 menu-item filter-starters">
                                <img src="/principal/img/Menu/pique.jpg" className="menu-img" />
                                <div className="menu-content">
                                    <a href="#">Pique</a>
                                    <span>Bs 30.95</span>
                                </div>
                                <div className="menu-ingredients">
                                    A delicate crab cake served on a toasted roll with lettuce and tartar sauce
                                </div>
                            </div>
                            <div className="col-lg-6 menu-item filter-salads">
                                <img src="/principal/img/Menu/sopa_mani.jpg" className="menu-img" />
                                <div className="menu-content">
                                    <a href="#">Sopa de Mani</a>
                                    <span>Bs 10.95</span>
                                </div>
                                <div className="menu-ingredients">
                                    Lorem, deren, trataro, filede, nerada
                                </div>
                            </div>
                            <div className="col-lg-6 menu-item filter-speciy">
                                <img src="/principal/img/Menu/kawi.jpg" className="menu-img" />
                                <div className="menu-content">
                                    <a href="#">Kawi</a>
                                    <span>Bs 9.95</span>
                                </div>
                                <div className="menu-ingredients">
                                    Grilled chicken with provolone, artichoke hearts, and roasted red pesto
                                </div>
                            </div>
                            <div className="col-lg-6 menu-item filter-starters">
                                <img src="/principal/img/Menu/chairo.jpg" className="menu-img" />
                                <div className="menu-content">
                                    <a href="#">Chairo</a>
                                    <span>$4.95</span>
                                </div>
                                <div className="menu-ingredients">
                                    Lorem, deren, trataro, filede, nerada
                                </div>
                            </div>
                            <div className="col-lg-6 menu-item filter-salads">
                                <img src="/principal/img/Menu/fricase.jpg" className="menu-img" />
                                <div className="menu-content">
                                    <a href="#">Fricase</a>
                                    <span>$9.95</span>
                                </div>
                                <div className="menu-ingredients">
                                    Fresh spinach, crisp romaine, tomatoes, and Greek olives
                                </div>
                            </div>
                            <div className="col-lg-6 menu-item filter-salads">
                                <img src="/principal/img/Menu/chicharon.jpg" className="menu-img" />
                                <div className="menu-content">
                                    <a href="#">Chicharon</a>
                                    <span>$9.95</span>
                                </div>
                                <div className="menu-ingredients">
                                    Fresh spinach with mushrooms, hard boiled egg, and warm bacon vinaigrette
                                </div>
                            </div>
                            <div className="col-lg-6 menu-item filter-speciy">
                                <img src="/principal/img/Menu/sajta.jpg" className="menu-img" />
                                <div className="menu-content">
                                    <a href="#">Sajta</a>
                                    <span>$12.95</span>
                                </div>
                                <div className="menu-ingredients">
                                    Plump lobster meat, mayo and crisp lettuce on a toasted bulky roll
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="specials" className="specials">
                    <div className="container" data-aos="fade-up">
                        <div className="section-title">
                            <h2>Especialidades</h2>
                            <p>Consulte Nuestra Especialidades</p>
                        </div>
                        <div className="row" data-aos="fade-up"
                            data-aos-delay={100}>
                            <div className="col-lg-3">
                                <ul className="nav nav-tabs flex-column">
                                    <li className="nav-item">
                                        <a className="nav-link active show" data-bs-toggle="tab" href="#tab-1">Chanka de pollo</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" data-bs-toggle="tab" href="#tab-2">Papas A la huacaina</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" data-bs-toggle="tab" href="#tab-3">Chajchu</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" data-bs-toggle="tab" href="#tab-4">Caldo de cardan</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" data-bs-toggle="tab" href="#tab-5">Ranga</a>
                                    </li>
                                </ul>
                            </div>
                            <div className="col-lg-9 mt-4 mt-lg-0">
                                <div className="tab-content">
                                    <div className="tab-pane active show" id="tab-1">
                                        <div className="row">
                                            <div className="col-lg-8 details order-2 order-lg-1">
                                                <h3>
                                                    Chanka de pollo</h3>
                                                <p className="fst-italic">Qui laudantium consequatur laborum sit qui ad sapiente dila parde sonata raqer a videna mareta paulona marka</p>
                                                <p>Et nobis maiores eius. Voluptatibus ut enim blanditiis atque harum sint. Laborum eos ipsum ipsa odit magni. Incidunt hic ut molestiae aut qui. Est repellat minima eveniet eius et quis magni nihil. Consequatur dolorem quaerat quos qui similique accusamus nostrum rem vero</p>
                                            </div>
                                            <div className="col-lg-4 text-center order-1 order-lg-2">
                                                <img src="/principal/img/Especiales/pollo.jpg"  className="img-fluid"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="tab-pane" id="tab-2">
                                        <div className="row">
                                            <div className="col-lg-8 details order-2 order-lg-1">
                                                <h3>Papas A la huacaina</h3>
                                                <p className="fst-italic">Qui laudantium consequatur laborum sit qui ad sapiente dila parde sonata raqer a videna mareta paulona marka</p>
                                                <p>Ea ipsum voluptatem consequatur quis est. Illum error ullam omnis quia et reiciendis sunt sunt est. Non aliquid repellendus itaque accusamus eius et velit ipsa voluptates. Optio nesciunt eaque beatae accusamus lerode pakto madirna desera vafle de nideran pal</p>
                                            </div>
                                            <div className="col-lg-4 text-center order-1 order-lg-2">
                                                <img src="/principal/img/Especiales/papas.jpg"  className="img-fluid"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="tab-pane" id="tab-3">
                                        <div className="row">
                                            <div className="col-lg-8 details order-2 order-lg-1">
                                                <h3>Chajchu</h3>
                                                <p className="fst-italic">Eos voluptatibus quo. Odio similique illum id quidem non enim fuga. Qui natus non sunt dicta dolor et. In asperiores velit quaerat perferendis aut</p>
                                                <p>Iure officiis odit rerum. Harum sequi eum illum corrupti culpa veritatis quisquam. Neque necessitatibus illo rerum eum ut. Commodi ipsam minima molestiae sed laboriosam a iste odio. Earum odit nesciunt fugiat sit ullam. Soluta et harum voluptatem optio quae</p>
                                            </div>
                                            <div className="col-lg-4 text-center order-1 order-lg-2">
                                                <img src="/principal/img/Especiales/chajchu.jpg"  className="img-fluid"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="tab-pane" id="tab-4">
                                        <div className="row">
                                            <div className="col-lg-8 details order-2 order-lg-1">
                                                <h3>Caldo de cardan</h3>
                                                <p className="fst-italic">Totam aperiam accusamus. Repellat consequuntur iure voluptas iure porro quis delectus</p>
                                                <p>Eaque consequuntur consequuntur libero expedita in voluptas. Nostrum ipsam necessitatibus aliquam fugiat debitis quis velit. Eum ex maxime error in consequatur corporis atque. Eligendi asperiores sed qui veritatis aperiam quia a laborum inventore</p>
                                            </div>
                                            <div className="col-lg-4 text-center order-1 order-lg-2">
                                                <img src="/principal/img/Especiales/caldo.jpg"  className="img-fluid"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="tab-pane" id="tab-5">
                                        <div className="row">
                                            <div className="col-lg-8 details order-2 order-lg-1">
                                                <h3>Ranga</h3>
                                                <p className="fst-italic">Omnis blanditiis saepe eos autem qui sunt debitis porro quia.</p>
                                                <p>Exercitationem nostrum omnis. Ut reiciendis repudiandae minus. Omnis recusandae ut non quam ut quod eius qui. Ipsum quia odit vero atque qui quibusdam amet. Occaecati sed est sint aut vitae molestiae voluptate vel</p>
                                            </div>
                                            <div className="col-lg-4 text-center order-1 order-lg-2">
                                                <img src="/principal/img/Especiales/ranga.jpg"  className="img-fluid"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="book-a-table" className="book-a-table">
                    <div className="container" data-aos="fade-up">
                        <div className="section-title">
                            <h2>Reservation</h2>
                            <p>Book a Table</p>
                        </div>
                        <form action="forms/book-a-table.php" method role="form" className="php-email-form" data-aos="fade-up"
                            data-aos-delay={100}>
                            <div className="row">
                                <div className="col-lg-4 col-md-6 form-group">
                                    <input type="text" name="name" className="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars"/>
                                    <div className="validate"/>
                                </div>
                                <div className="col-lg-4 col-md-6 form-group mt-3 mt-md-0">
                                    <input type="email" className="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email"/>
                                    <div className="validate"/>
                                </div>
                                <div className="col-lg-4 col-md-6 form-group mt-3 mt-md-0">
                                    <input type="text" className="form-control" name="phone" id="phone" placeholder="Your Phone" data-rule="minlen:4" data-msg="Please enter at least 4 chars"/>
                                    <div className="validate"/>
                                </div>
                                <div className="col-lg-4 col-md-6 form-group mt-3">
                                    <input type="text" name="date" className="form-control" id="date" placeholder="Date" data-rule="minlen:4" data-msg="Please enter at least 4 chars"/>
                                    <div className="validate"/>
                                </div>
                                <div className="col-lg-4 col-md-6 form-group mt-3">
                                    <input type="text" className="form-control" name="time" id="time" placeholder="Time" data-rule="minlen:4" data-msg="Please enter at least 4 chars"/>
                                    <div className="validate"/>
                                </div>
                                <div className="col-lg-4 col-md-6 form-group mt-3">
                                    <input type="number" className="form-control" name="people" id="people" placeholder="# of people" data-rule="minlen:1" data-msg="Please enter at least 1 chars"/>
                                    <div className="validate"/>
                                </div>
                            </div>
                            <div className="form-group mt-3">
                                <textarea className="form-control" name="message"
                                    rows={5}
                                    placeholder="Message"
                                    defaultValue={""}/>
                                <div className="validate"/>
                            </div>
                            <div className="mb-3">
                                <div className="loading">Loading</div>
                                <div className="error-message"/>
                                <div className="sent-message">Your booking request was sent. We will call back or send an Email to confirm your reservation. Thank you!</div>
                            </div>
                            <div className="text-center">
                                <button type="submit">Book a Table</button>
                            </div>
                        </form>
                    </div>
                </section>
                {/*<section id="gallery" className="gallery">/*}
                    <div className="container" data-aos="fade-up">
                        <div className="section-title">
                            <h2>Gallery</h2>
                            <p>Some photos from Our Restaurant</p>
                        </div>
                    </div>
                    <div className="container-fluid" data-aos="fade-up"
                        data-aos-delay={100}>
                        <div className="row g-0">
                            <div className="col-lg-3 col-md-4">
                                <div className="gallery-item">
                                    <a href="assets/img/gallery/gallery-1.jpg" className="gallery-lightbox" data-gall="gallery-item">
                                        <img src="assets/img/gallery/gallery-1.jpg"  className="img-fluid"/>
                                    </a>
                                </div>
                            </div>
                            <div className="col-lg-3 col-md-4">
                                <div className="gallery-item">
                                    <a href="assets/img/gallery/gallery-2.jpg" className="gallery-lightbox" data-gall="gallery-item">
                                        <img src="assets/img/gallery/gallery-2.jpg"  className="img-fluid"/>
                                    </a>
                                </div>
                            </div>
                            <div className="col-lg-3 col-md-4">
                                <div className="gallery-item">
                                    <a href="assets/img/gallery/gallery-3.jpg" className="gallery-lightbox" data-gall="gallery-item">
                                        <img src="assets/img/gallery/gallery-3.jpg"  className="img-fluid"/>
                                    </a>
                                </div>
                            </div>
                            <div className="col-lg-3 col-md-4">
                                <div className="gallery-item">
                                    <a href="assets/img/gallery/gallery-4.jpg" className="gallery-lightbox" data-gall="gallery-item">
                                        <img src="assets/img/gallery/gallery-4.jpg"  className="img-fluid"/>
                                    </a>
                                </div>
                            </div>
                            <div className="col-lg-3 col-md-4">
                                <div className="gallery-item">
                                    <a href="assets/img/gallery/gallery-5.jpg" className="gallery-lightbox" data-gall="gallery-item">
                                        <img src="assets/img/gallery/gallery-5.jpg"  className="img-fluid"/>
                                    </a>
                                </div>
                            </div>
                            <div className="col-lg-3 col-md-4">
                                <div className="gallery-item">
                                    <a href="assets/img/gallery/gallery-6.jpg" className="gallery-lightbox" data-gall="gallery-item">
                                        <img src="assets/img/gallery/gallery-6.jpg"  className="img-fluid"/>
                                    </a>
                                </div>
                            </div>
                            <div className="col-lg-3 col-md-4">
                                <div className="gallery-item">
                                    <a href="assets/img/gallery/gallery-7.jpg" className="gallery-lightbox" data-gall="gallery-item">
                                        <img src="assets/img/gallery/gallery-7.jpg"  className="img-fluid"/>
                                    </a>
                                </div>
                            </div>
                            <div className="col-lg-3 col-md-4">
                                <div className="gallery-item">
                                    <a href="assets/img/gallery/gallery-8.jpg" className="gallery-lightbox" data-gall="gallery-item">
                                        <img src="assets/img/gallery/gallery-8.jpg"  className="img-fluid"/>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                                    </section>*/}
                <section id="contact" className="contact">
                    <div className="container" data-aos="fade-up">
                        <div className="section-title">
                            <h2>Contact</h2>
                            <p>Contact Us</p>
                        </div>
                    </div>
                    <div data-aos="fade-up">
                        <iframe style={
                                {
                                    border: 0,
                                    width: '100%',
                                    height: 350
                                }
                            }
                            src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621"
                            frameBorder={0}
                            allowFullScreen/>
                    </div>
                    <div className="container" data-aos="fade-up">
                        <div className="row mt-5">
                            <div className="col-lg-4">
                                <div className="info">
                                    <div className="address">
                                        <i className="bi bi-geo-"/>
                                        <h4>Location:</h4>
                                        <p>A108 Adam Street, New York, NY 535022</p>
                                    </div>
                                    <div className="open-hours">
                                        <i className="bi bi-clock"/>
                                        <h4>Open Hours:</h4>
                                        <p>
                                            Monday-Saturday:<br/>
                                            11:00 AM - 2300 PM
                                        </p>
                                    </div>
                                    <div className="email">
                                        <i className="bi bi-envelope"/>
                                        <h4>Email:</h4>
                                        <p>info@example.com</p>
                                    </div>
                                    <div className="phone">
                                        <i className="bi bi-phone"/>
                                        <h4>Call:</h4>
                                        <p>+1 5589 55488 55s</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-8 mt-5 mt-lg-0">
                                <form action="forms/contact.php" method role="form" className="php-email-form">
                                    <div className="row">
                                        <div className="col-md-6 form-group">
                                            <input type="text" name="name" className="form-control" id="name" placeholder="Your Name" required/>
                                        </div>
                                        <div className="col-md-6 form-group mt-3 mt-md-0">
                                            <input type="email" className="form-control" name="email" id="email" placeholder="Your Email" required/>
                                        </div>
                                    </div>
                                    <div className="form-group mt-3">
                                        <input type="text" className="form-control" name="subject" id="subject" placeholder="Subject" required/>
                                    </div>
                                    <div className="form-group mt-3">
                                        <textarea className="form-control" name="message"
                                            rows={8}
                                            placeholder="Message"
                                            required
                                            defaultValue={""}/>
                                    </div>
                                    <div className="my-3">
                                        <div className="loading">Loading</div>
                                        <div className="error-message"/>
                                        <div className="sent-message">Your message has been sent. Thank you!</div>
                                    </div>
                                    <div className="text-center">
                                        <button type="submit">Send Message</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>

            </div>


        )
    }
}
export default withTracker((props) => {
    const optionspublications={
        category:categorySelect.get()
    }
    const suscriptionPublications = Meteor.subscribe('publications',optionspublications,'getAllPublications')
    const subcriptionCategory = Meteor.subscribe('category',{},'getCategory')
    const categorys= categoryClass.find().fetch()
    const publications = publicationsClass.find({},{sort:{createdAt:-1}}).fetch()
    const publicationsLimitTwo = publicationsClass.find({},{sort:{createdAt:-1},limit:2}).fetch()
    return {publications, suscriptionPublications,categorys,subcriptionCategory,publicationsLimitTwo}
})(Main)
