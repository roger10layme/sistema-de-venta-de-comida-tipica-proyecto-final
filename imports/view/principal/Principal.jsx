import React, {Component} from 'react'


import {Link, Switch} from 'react-router-dom'
import SwitchRoutes from '/imports/routes/SwitchRoutes'


export default class Principal extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loader: true
        }
    }
    componentDidMount() {
        import '/imports/assets/principal/css'
        import '/imports/assets/principal/js'
        // import desinPrincipal from'/imports/assets/principal/js/main'
        const mthis = this
        setTimeout(function () {
            mthis.setState({loader: false})
            // desinPrincipal()
            $('#preloader-active').fadeOut('slow');
        }, 1000)

    }

    render() {
        const {routes} = this.props

        return (
            <div> {
                this.state.loader ? <div id="preloader-active">
                    <div className="preloader d-flex align-items-center justify-content-center">
                        <div className="preloader-inner position-relative">
                            <div className="preloader-circle"></div>
                            <div className="preloader-img pere-text">
                                <img src="/principal/img/resloader.png" alt=""/>
                            </div>
                        </div>
                    </div>
                </div> : <div>

                    <header id="header" className="fixed-top d-flex align-items-cente">
                        <div className="container-fluid container-xl d-flex align-items-center justify-content-lg-between">
                            <h1 className="logo me-auto me-lg-0">
                                <a href="index.html">Restaurante</a>
                            </h1>
                            <nav id="navbar" className="navbar order-last order-lg-0">
                                <ul>
                                    <li>
                                        <a className="nav-link scrollto active" href="/">Home</a>
                                    </li>
                                    <li>
                                        <a className="nav-link scrollto" href="#about">About</a>
                                    </li>
                                    <li>
                                        <a className="nav-link scrollto" href="#menu">Menu</a>
                                    </li>
                                    <li>
                                        <a className="nav-link scrollto" href="#specials">Especiales</a>
                                    </li>
                                    {/*<li>
                                        <a className="nav-link scrollto" href="#events">Eventos</a>
                                    </li>*/}
                                    {/*<li>
                                        <a className="nav-link scrollto" href="#gallery">Galeria</a>
                                    </li>*/}
                                    <li>
                                        <a className="nav-link scrollto" href="#contact">Contactos</a>
                                    </li>
                                    <li className="button-header margin-left ">
                                        <Link to="/bienvenido/registro" className="btn">
                                            Registrame</Link>
                                    </li>

                                    <li className="button-header">
                                        <Link to="/bienvenido/login" className="btn3">
                                            Iniciar sesion</Link>
                                    </li>
                                </ul>
                                <i className="bi bi-list mobile-nav-toggle"/>
                            </nav>
                            <a href="#book-a-table" className="book-a-table-btn scrollto d-none d-lg-flex">Book a table</a>
                        </div>
                    </header>
                    <main>
                        <Switch> {
                            routes.map((route, i) => (
                                <SwitchRoutes key={i}
                                    {...route}/>
                            ))
                        } </Switch>
                    </main>
                    <footer id="footer">
                        <div className="footer-top">
                            <div className="container">
                                <div className="row">
                                    <div className="col-lg-3 col-md-6">
                                        <div className="footer-info">
                                            <h3>Restaurantly</h3>
                                            <p>
                                                A108 Adam Street
                                                <br/>
                                                NY 535022, USA<br/><br/>
                                                <strong>Phone:</strong>
                                                +1 5589 55488 55<br/>
                                                <strong>Email:</strong>
                                                info@example.com<br/>
                                            </p>
                                            <div className="social-links mt-3">
                                                <a href="#" className="twitter"><i className="bx bxl-twitter"/></a>
                                                <a href="#" className="facebook"><i className="bx bxl-facebook"/></a>
                                                <a href="#" className="instagram"><i className="bx bxl-instagram"/></a>
                                                <a href="#" className="google-plus"><i className="bx bxl-skype"/></a>
                                                <a href="#" className="linkedin"><i className="bx bxl-linkedin"/></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-2 col-md-6 footer-links">
                                        <h4>Useful Links</h4>
                                        <ul>
                                            <li><i className="bx bx-chevron-right"/>
                                                <a href="#">Home</a>
                                            </li>
                                            <li><i className="bx bx-chevron-right"/>
                                                <a href="#">About us</a>
                                            </li>
                                            <li><i className="bx bx-chevron-right"/>
                                                <a href="#">Services</a>
                                            </li>
                                            <li><i className="bx bx-chevron-right"/>
                                                <a href="#">Terms of service</a>
                                            </li>
                                            <li><i className="bx bx-chevron-right"/>
                                                <a href="#">Privacy policy</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="col-lg-3 col-md-6 footer-links">
                                        <h4>Our Services</h4>
                                        <ul>
                                            <li><i className="bx bx-chevron-right"/>
                                                <a href="#">Web Design</a>
                                            </li>
                                            <li><i className="bx bx-chevron-right"/>
                                                <a href="#">Web Development</a>
                                            </li>
                                            <li><i className="bx bx-chevron-right"/>
                                                <a href="#">Product Management</a>
                                            </li>
                                            <li><i className="bx bx-chevron-right"/>
                                                <a href="#">Marketing</a>
                                            </li>
                                            <li><i className="bx bx-chevron-right"/>
                                                <a href="#">Graphic Design</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="col-lg-4 col-md-6 footer-newsletter">
                                        <h4>Our Newsletter</h4>
                                        <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
                                        <form method="post">
                                            <input type="email" name="email"/><input type="submit" defaultValue="Subscribe"/>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="container">
                            <div className="copyright">
                                © Copyright
                                <strong>
                                    <span>Restaurantly</span>
                                </strong>. All Rights Reserved
                            </div>
                            <div className="credits">
                                <a href="https://bootstrapmade.com/">BootstrapMade</a>
                            </div>
                        </div>
                    </footer>

                    {/*<div>
                            <div id="preloader" />
                            <a href="#" className="back-to-top d-flex align-items-center justify-content-center"><i className="bi bi-arrow-up-short" /></a>
                        </div>*/} </div>
            } </div>

        )
    }
}
