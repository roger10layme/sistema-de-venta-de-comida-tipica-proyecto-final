import React, { Component } from 'react'
import { categoryClass } from '../../models/category/class';
import { withTracker } from 'meteor/react-meteor-data'
import { Meteor } from 'meteor/meteor';
import {publicationsClass} from '/imports/models/publicacions/class' ///importar la clase de nuestra coleccion o tabla
import DatePicker from "react-datepicker";
import FileUpload from '../../componets/FileUpload'
import uploadFiles from '../../utils/upload'

class CreatePublication extends Component {
  constructor(props) {
    super(props)
    this.state = {
      form: {
        title: null,
        description: null,
        source:null,
        phone: null,
        price: null,
        starDate: new Date(),
        endDate: new Date(),
        image: null,
        category: null,
      },
      typefile:'image',
      videourl:null,
      errors: {}
    }
  }
  valideteform = () => {
    const { form } = this.state //obtener la propiedad form de this state 
    let errorsform = {}//objeto vacio 
    let formsIsValid = true//verificar si el formulario es valido o no 
    if (!form.title) {
      formsIsValid = false
      errorsform['title'] = "El titulo no puede estar vacio"
    }
    if (!form.description) {
      formsIsValid = false
      errorsform['description'] = "la descripcion  no puede estar vacio"
    }
    if (!form.source) {
      formsIsValid = false
      errorsform['source'] = "El origen  no puede estar vacio"
    }
    if (!form.phone) {
      formsIsValid = false
      errorsform['phone'] = "El telefono no puede estar vacio"
    }
    if (!form.price) {
      formsIsValid = false
      errorsform['price'] = "El precio no puede estar vacio"
    }
    if (!form.starDate) {
      formsIsValid = false
      errorsform['starDate'] = "El starDate no puede estar vacio"
    }
    if (!form.endDate) {
      formsIsValid = false
      errorsform['endDate'] = "El endDate no puede estar vacio"
    }


    this.setState({ errors: errorsform })//cambiamos el valo de la propiedad errors  en this.state con los nuebos valores con lo valores que estan en errorsform
    return formsIsValid
  }

  //createnewPublications(e){  //no tiene acceso a lo que es las variables de adentro 
  createnewPublications = (e) => {  // si va a tener acceso ala variables de los componentes
    e.preventDefault()
    if (this.valideteform()) {
      //alert('enviando formulario')
      //console.log(this.state.form)
      const newpublicacion = new publicationsClass()
      const { form } = this.state //obtener la propiedad form de this state
      form.price = parseInt(form.price)

      const { form: { image } } = this.state
      const uf = new uploadFiles(image.file, image.self)
      const mthis=this
      uf.newUpload(function (error, success) {
        if (error) {
          console.log('**********************')
          console.log(error)
          console.log('**********************')
        } else {
          form.image = success._id
          newpublicacion.callMethod('newPublication', form, (error, result) => {
            if (error) {
              alert(error)
            } else {
              alert(result)
              document.getElementById("newPublication").reset()
              mthis.setState({typefile:'video'})
            }
          })

        }
      })


    } else {
      alert('El formulario tiene errores')
    }

  }
  changeTextInput = (e) => { //(e)esta llegando a todas la propiedades que pueda tener input
    const value = e.target.value //recupera el valor que se escribe en impnput
    const property = e.target.name //recupera la propiedad name del input

    //precstate esta recuperando el valor anterior de this.state
    //precState from esta sacando  todas la prpiedades de form 
    this.setState(prevState => (
      {
        form: {
          ...prevState.form,
          [property]: value,//[property] esta variable property la ponemos en corchetes para que sepa em metodo que es prpoiedad va hacer dinamica y va tomar los distintos valores 
        }
      }
    ))
  }
  changeDateImput = (type, date) => {
    this.setState(prevState => (
      {
        form: {
          ...prevState.form,
          [type]: date,
        }
      }
    ))
  }
  changeSelectInput = (e) => {
    const value = e.target.value
    this.setState(prevState => (
      {
        form: {
          ...prevState.form,
          category: value,
        }
      }
    ))
  }
  changeFileInput = (data) => {
    //console.log(data)
    const inputfile = data.file
    if (inputfile && inputfile[0]) {
      const indextext=inputfile[0].type.lastIndexOf('/')
      const typefile =inputfile[0].type.slice(0,indextext)
      if(typefile == 'image'){
        this.setState({typefile:'image'})
        let reader = new FileReader()
        reader.onload = function (v) {
          $('#previewimge').attr('src', v.target.result)
        }
        reader.readAsDataURL(inputfile[0])
      }else{
        this.setState({typefile:'video'})
        let file=inputfile[0]
        let blobURL = URL.createObjectURL(file)
        this.setState({videourl:blobURL})
        this.setState({typefile:'video'})
        //$('#previewvideo').src =blobURL

      }
      this.setState(prevState => (
        {
          form: {
            ...prevState.form,
            image: data,
          }
        }
      ))
    }

  }
  render() {
    const { errors,typefile,videourl} = this.state  ///recuperar errors de this,state
    const { categorys, subcriptionCategory } = this.props
    return (
      <div>
        <section className="section">
          <div className="section-body">
            <div className="row">
              <div className="col-12 col-md-6 col-lg-12">
                <div className="card">
                  <div className="card-header">
                    <h4>Crear Nueva Publicacion </h4>
                  </div>
                  {!subcriptionCategory.ready() ?
                    <h1>Cargando!!!</h1>
                    :
                    <div className="card-body">
                      <form onSubmit={this.createnewPublications} id="newPublication">
                        <div className="row">
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Titulo de la Publicacion</label>
                              <input type="text" className={errors.title ? "form-control is-invalid" : "form-control"} name={'title'} onChange={this.changeTextInput} autoComplete="off" />
                              {
                                errors.title ?
                                  <div className="invalid-feedback">
                                    {errors.title}</div> : null
                              }
                            </div>
                            <div className="form-group">
                              <label>Descripcion</label>
                              <input type="text" className={errors.description ? "form-control invoice-input is-invalid" : "form-control invoice-input"} name={'description'} onChange={this.changeTextInput}  autoComplete="off"/>
                              {errors.description ? <div className="invalid-feedback">{errors.description}</div> : null}
                            </div>
                            <div className="form-group">
                              <label>Origen</label>
                              <input type="text" className={errors.source ? "form-control invoice-input is-invalid" : "form-control invoice-input"} name={'source'} onChange={this.changeTextInput} autoComplete="off" />
                              {errors.source ? <div className="invalid-feedback">{errors.source}</div> : null}
                            </div>
                            <div className="form-group">
                              <label>Numero de Telefono</label>
                              <div className="input-group">
                                <div className="input-group-prepend">
                                  <div className="input-group-text">
                                    <i className="fas fa-phone"></i>
                                  </div>
                                </div>
                                <input type="text" className={errors.phone ? "form-control phone-number is-invalid" : "form-control phone-number"} name={'phone'} onChange={this.changeTextInput} autoComplete="off" />
                                {
                                  errors.phone ?
                                    <div className="invalid-feedback">
                                      {errors.phone}</div> : null
                                }
                              </div>
                            </div>
                            <div className="form-group">
                              <label>Precio</label>
                              <div className="input-group">
                                <div className="input-group-prepend">
                                  <div className="input-group-text">
                                    $
                                  </div>
                                </div>
                                <input type="text" className={errors.price ? "form-control currency is-invalid" : "form-control currency"} name={'price'} onChange={this.changeTextInput} autoComplete="off" />
                                {
                                  errors.price ?
                                    <div className="invalid-feedback">
                                      {errors.price}</div> : null
                                }
                              </div>
                            </div>
                            <div className="form-group">
                              <label>Selecione una categoria</label>
                              <select className="form-control" onChange={this.changeSelectInput}>
                                <option defaultValue >Selecione una categoria</option>
                                {
                                  categorys.map((category, key) => {
                                    return <option key={`category ${key}`} value={category._id}>{category.name}</option>
                                  })
                                }
                              </select>
                            </div>
                            <div className="form-group">
                              <label>Archibo</label>
                              {/*<input type="file" className='form-control' name={'file'} onChange={this.changefileInput} />*/}
                              <FileUpload changeFileInput={this.changeFileInput} accept=".jpeg, .jpg , .png, .mp4"/>
                            </div>
                            <div className="form-group">
                              <label>Fecha de Publicacion </label>
                              {/*<input type="text" className={errors.starDate?"form-control datemask is-invalid":"form-control datemask " }placeholder="YYYY/MM/DD" name={'starDate'} onChange={this.changeTextInput} autoComplete="off" />*/}
                              <div>
                                <DatePicker selected={this.state.form.starDate} className={errors.starDate ? "form-control datemask is-invalid" : "form-control datemask "} name={'starDate'}
                                  onChange={date => {
                                    this.changeDateImput('starDate', date)
                                  }}
                                />
                                {errors.starDate ? <div className="invalid-feedback" style={{ display: 'block' }}>{errors.starDate}</div> : null}
                              </div>

                            </div>
                            <div className="form-group">
                              <label>Fecha Culminacion </label>
                              {/*<input type="text" className={errors.endDate?"form-control datemask is-invalid":"form-control datemask"}placeholder="YYYY/MM/DD" name={'endDate'} onChange={this.changeTextInput} autoComplete="off" />*/}
                              <div>
                                <DatePicker selected={this.state.form.endDate} className={errors.endDate ? "form-control datemask is-invalid" : "form-control datemask"} name={'endDate'}
                                  onChange={date => {
                                    this.changeDateImput('endDate', date)
                                  }}
                                />
                                {errors.endDate ? <div className="invalid-feedback" style={{ display: 'block' }}>{errors.endDate}</div> : null}
                              </div>

                            </div>

                          </div>
                          <div className="col-md-6 d-flex justify-content-center">
                            <div className="author-box-center">
                              {typefile == 'image'?
                                <img alt="image" src="/dashboard/img/users/img-icons1.png" id="previewimge" className="rounded-circle author-box-picture" style={{ width: '100%', heigh: '500' }} />
                              :
                                <video className="card-img rounded-0" id="previewvideo" controls>
                                  <source src={videourl} />
                                </video>

                              }
                              
                              <div className="clearfix" />
                              <div className="author-box-name">

                              </div>
                              <div className="author-box-job d-flex justify-content-center">Vista previa</div>
                            </div>

                          </div>
                        </div>
                        <button type="submit" className="btn btn-icon icon-left btn-primary"><i className="far fa-edit"></i> Guardar Publicacion</button>
                      </form>
                    </div>
                  }
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
  }
}

export default withTracker((props)=>{
  const subcriptionCategory = Meteor.subscribe('category',{},'getCategory')
  const categorys= categoryClass.find().fetch()
  console.log(categorys)
  return {categorys,subcriptionCategory}
})(CreatePublication)