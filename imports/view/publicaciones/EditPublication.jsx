import React, { Component } from 'react'
import { categoryClass } from '../../models/category/class';
import { withTracker } from 'meteor/react-meteor-data'
import { Meteor } from 'meteor/meteor';
import {publicationsClass} from '/imports/models/publicacions/class' ///importar la clase de nuestra coleccion o tabla
import DatePicker from "react-datepicker";
import FileUpload from '../../componets/FileUpload'
import uploadFiles from '../../utils/upload'

class EditPulication extends Component {
    constructor(props) {
        super(props)
        this.state = {
            form: {
                title:props.getpublication.title,
                description:props.getpublication.description,
                source:props.getpublication.source,
                phone:props.getpublication.phone,
                price:props.getpublication.price,
                starDate: props.getpublication.starDate,
                endDate: props.getpublication.endDate,
                image: null,
                category: props.getpublication.idCategory._str
            },
            errors: {}
        }
    }
    valideteform = () => {
        const { form } = this.state //obtener la propiedad form de this state 
        let errorsform = {}//objeto vacio 
        let formsIsValid = true//verificar si el formulario es valido o no 
        if (!form.title) {
            formsIsValid = false
            errorsform['title'] = "El titulo no puede estar vacio"
        }
        if (!form.description) {
            formsIsValid = false
            errorsform['description'] = "la descripcion  no puede estar vacio"
        }
        if (!form.source) {
            formsIsValid = false
            errorsform['source'] = "el origen  no puede estar vacio"
        }
        if (!form.phone) {
            formsIsValid = false
            errorsform['phone'] = "El telefono no puede estar vacio"
        }
        if (!form.price) {
            formsIsValid = false
            errorsform['price'] = "El precio no puede estar vacio"
        }
        if (!form.starDate) {
            formsIsValid = false
            errorsform['starDate'] = "El starDate no puede estar vacio"
        }
        if (!form.endDate) {
            formsIsValid = false
            errorsform['endDate'] = "El endDate no puede estar vacio"
        }


        this.setState({ errors: errorsform })//cambiamos el valo de la propiedad errors  en this.state con los nuebos valores con lo valores que estan en errorsform
        return formsIsValid
    }

    
    editPublications = (e) => {  // si va a tener acceso ala variables de los componentes
        e.preventDefault()
        const {history}=this.props
        if (this.valideteform()) {
            //alert('enviando formulario')
            //console.log(this.state.form)
            const editpublicacion = this.props.getpublication
            const { form } = this.state //obtener la propiedad form de this state
            form.price = parseInt(form.price)
            if(this.state.form.image){
                
                
                const { form: { image } } = this.state
                const uf = new uploadFiles(image.file, image.self)
                uf.newUpload(function (error, success) {
                    if (error) {
                        console.log('**********************')
                        console.log(error)
                        console.log('**********************')
                    } else {
                        form.image = success._id
                        editpublicacion.callMethod('editPublication', form, (error, result) => {
                            if (error) {
                                alert(error)
                            } else {
                                alert(result)
                                history.push('/dashboard/user-publications')
                                
                            }
                        })
    
                    }
                })
                
            }else{
                editpublicacion.callMethod('editPublication', form, (error, result) => {
                    if (error) {
                        alert(error)
                    } else {
                        alert(result)
                        history.push('/dashboard/user-publications')
                           
                            
                        
                        
                    }
                })
            }
            return false
        } else {
            alert('El formulario tiene errores')
        }

    }
    changeTextInput = (e) => { //(e)esta llegando a todas la propiedades que pueda tener input
        const value = e.target.value //recupera el valor que se escribe en impnput
        const property = e.target.name //recupera la propiedad name del input

        //precstate esta recuperando el valor anterior de this.state
        //precState from esta sacando  todas la prpiedades de form 
        this.setState(prevState => (
            {
                form: {
                    ...prevState.form,
                    [property]: value,//[property] esta variable property la ponemos en corchetes para que sepa em metodo que es prpoiedad va hacer dinamica y va tomar los distintos valores 
                }
            }
        ))
    }
    changeDateImput = (type, date) => {
        this.setState(prevState => (
            {
                form: {
                    ...prevState.form,
                    [type]: date,
                }
            }
        ))
    }
    changeSelectInput = (e) => {
        const value = e.target.value
        this.setState(prevState => (
            {
                form: {
                    ...prevState.form,
                    category: value,
                }
            }
        ))
    }
    changeFileInput = (data) => {
        //console.log(data)
        const inputfile = data.file
        if (inputfile && inputfile[0]) {
            let reader = new FileReader()
            reader.onload = function (v) {
                $('#previewimge').attr('src', v.target.result)
            }
            reader.readAsDataURL(inputfile[0])
            this.setState(prevState => (
                {
                    form: {
                        ...prevState.form,
                        image: data,
                    }
                }
            ))
        }

    }
    render() {
        const { errors,form} = this.state  ///recuperar errors de this,state
        const {categorys,subcriptionCategory,getpublication,subcriptionPublication}= this.props
        
        return (
            <div>
                <section className="section">
                    <div className="section-body">
                        <div className="row">
                            <div className="col-12 col-md-6 col-lg-12">
                                <div className="card">
                                    <div className="card-header">
                                        <h4>Editar Publicacion </h4>
                                    </div>
                                    {!subcriptionCategory.ready() && subcriptionPublication.ready()?
                                        <h1>Cargando!!!</h1>
                                        :
                                        <div className="card-body">
                                            <form onSubmit={this.editPublications} id="editPublication">
                                                <div className="row">
                                                    <div className="col-md-6">
                                                        <div className="form-group">
                                                            <label>Titulo de la Publicacion</label>
                                                            <input type="text" className={errors.title ? "form-control is-invalid" : "form-control"} value={form.title} name={'title'} onChange={this.changeTextInput} autoComplete="off" />
                                                            {
                                                                errors.title ?
                                                                    <div className="invalid-feedback">
                                                                        {errors.title}</div> : null
                                                            }
                                                        </div>
                                                        <div className="form-group">
                                                            <label>Descripcion</label>
                                                            <input type="text"  value={form.description} className={errors.description ? "form-control invoice-input is-invalid" : "form-control invoice-input"} name={'description'} onChange={this.changeTextInput} />
                                                            {errors.description ? <div className="invalid-feedback">{errors.description}</div> : null}
                                                        </div>
                                                        <div className="form-group">
                                                            <label>Origen</label>
                                                            <input type="text"  value={form.source} className={errors.source ? "form-control invoice-input is-invalid" : "form-control invoice-input"} name={'source'} onChange={this.changeTextInput} />
                                                            {errors.source ? <div className="invalid-feedback">{errors.source}</div> : null}
                                                        </div>
                                                        <div className="form-group">
                                                            <label>Numero de Telefono</label>
                                                            <div className="input-group">
                                                                <div className="input-group-prepend">
                                                                    <div className="input-group-text">
                                                                        <i className="fas fa-phone"></i>
                                                                    </div>
                                                                </div>
                                                                <input type="text" value={form.phone} className={errors.phone ? "form-control phone-number is-invalid" : "form-control phone-number"} name={'phone'} onChange={this.changeTextInput} autoComplete="off" />
                                                                {
                                                                    errors.phone ?
                                                                        <div className="invalid-feedback">
                                                                            {errors.phone}</div> : null
                                                                }
                                                            </div>
                                                        </div>
                                                        <div className="form-group">
                                                            <label>Precio</label>
                                                            <div className="input-group">
                                                                <div className="input-group-prepend">
                                                                    <div className="input-group-text">
                                                                        $
                                                                    </div>
                                                                </div>
                                                                <input type="text"  value={form.price} className={errors.price ? "form-control currency is-invalid" : "form-control currency"} name={'price'} onChange={this.changeTextInput} autoComplete="off" />
                                                                {
                                                                    errors.price ?
                                                                        <div className="invalid-feedback">
                                                                            {errors.price}</div> : null
                                                                }
                                                            </div>
                                                        </div>
                                                        <div className="form-group">
                                                            <label>Selecione una categoria</label>
                                                            <select className="form-control" value={form.category} onChange={this.changeSelectInput}>
                                                                <option defaultValue >Selecione una categoria</option>
                                                                {
                                                                    categorys.map((category, key) => {
                                                                        return <option key={`category ${key}`} value={category._id}>{category.name}</option>
                                                                    })
                                                                }
                                                            </select>
                                                        </div>
                                                        <div className="form-group">
                                                            <label>Archibo</label>
                                                            {/*<input type="file" className='form-control' name={'file'} onChange={this.changefileInput} />*/}
                                                            <FileUpload changeFileInput={this.changeFileInput} />
                                                        </div>
                                                        <div className="form-group">
                                                            <label>Fecha de Publicacion </label>
                                                            {/*<input type="text" className={errors.starDate?"form-control datemask is-invalid":"form-control datemask " }placeholder="YYYY/MM/DD" name={'starDate'} onChange={this.changeTextInput} autoComplete="off" />*/}
                                                            <div>
                                                                <DatePicker selected={form.starDate} className={errors.starDate ? "form-control datemask is-invalid" : "form-control datemask "} name={'starDate'}
                                                                    onChange={date => {
                                                                        this.changeDateImput('starDate', date)
                                                                    }}
                                                                />
                                                                {errors.starDate ? <div className="invalid-feedback" style={{ display: 'block' }}>{errors.starDate}</div> : null}
                                                            </div>

                                                        </div>
                                                        <div className="form-group">
                                                            <label>Fecha Culminacion </label>
                                                            {/*<input type="text" className={errors.endDate?"form-control datemask is-invalid":"form-control datemask"}placeholder="YYYY/MM/DD" name={'endDate'} onChange={this.changeTextInput} autoComplete="off" />*/}
                                                            <div>
                                                                <DatePicker selected={form.endDate} className={errors.endDate ? "form-control datemask is-invalid" : "form-control datemask"} name={'endDate'}
                                                                    onChange={date => {
                                                                        this.changeDateImput('endDate', date)
                                                                    }}
                                                                />
                                                                {errors.endDate ? <div className="invalid-feedback" style={{ display: 'block' }}>{errors.endDate}</div> : null}
                                                            </div>

                                                        </div>

                                                    </div>
                                                    <div className="col-md-6 d-flex justify-content-center">
                                                        <div className="author-box-center">
                                                            <img alt="image" src={getpublication.urlfile} id="previewimge" className="rounded-circle author-box-picture" style={{ width: '100%', heigh: '500' }} />
                                                            <div className="clearfix" />
                                                            <div className="author-box-name">

                                                            </div>
                                                            <div className="author-box-job d-flex justify-content-center">Vista previa</div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <button type="submit" className="btn btn-icon icon-left btn-primary"><i className="far fa-edit"></i> Primary</button>
                                            </form>
                                        </div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

export default withTracker((props)=>{
    const {location:{state:{publication}}}=props
    const subcriptionPublication = Meteor.subscribe('publications',{pub:publication},'getOnePublication')
    const subcriptionCategory = Meteor.subscribe('category',{},'getCategory')
    const categorys= categoryClass.find().fetch()
    const getpublication= publicationsClass.findOne()
    return {categorys,subcriptionCategory,getpublication,subcriptionPublication}
})(EditPulication)