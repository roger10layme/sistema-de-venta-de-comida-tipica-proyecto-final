import React, { Component } from 'react'
import {Link}  from  'react-router-dom'
import { Meteor } from 'meteor/meteor';

export default class Signin extends Component {
    constructor(props){
        super(props)
        this.state={
            username:'',
            password:''
        }
    }
    SubmitForlogin =(e) =>{
        e.preventDefault()
        const mthis=this
        Meteor.loginWithPassword(this.state.username,this.state.password,function(error){
            if(error)
                console.log(error)
            else
                //mthis.props.history.push('/dashboard/home')
                window.location.replace('/dashboard/home')
        })
    }

    componentDidMount(){
        import '/imports/assets/principal/js/jquery.vide'
        import initVideoBack from '/imports/assets/principal/js/initVideoBack'
        initVideoBack()
    }
    render() {
        return (
            <div>
                <main className="login-body" data-vide-bg="/principal/video/login-bg.mp4">                   
                    <form className="form-default" onSubmit={this.SubmitForlogin}>
                        <div className="login-form">                      
                            {/*<div className="logo-login">
                                <a href="index.html"><img src="/principal/img/loder.png" alt=""/></a>
                            </div>*/}
                            <h2>Login Here</h2>
                            <div className="form-input">
                                <label htmlFor="name">Email</label>
                                <input type="text" name="email" placeholder="Email" onChange={e=> this.setState({username:e.target.value})} autoComplete="off" />
                            </div>
                            <div className="form-input">
                                <label htmlFor="name">Password</label>
                                <input type="password" name="password" placeholder="Password" onChange={e=> this.setState({password:e.target.value})} autoComplete="off"/>
                            </div>
                            <div className="form-input pt-30">
                                <input type="submit" name="submit" value="Iniciar sesion "/>
                            </div>

                            
                            <a href="#" className="forget">Forget Password</a>
                          
                            <a href="register.html" className="registration">Registration</a>
                        </div>
                    </form>
       
                </main>
            </div>
        )
    }
}
