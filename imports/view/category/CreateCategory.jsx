import React, { Component } from 'react'
import {categoryClass }from '/imports/models/category/class'

export default class CreateCategory extends Component {
    constructor(props){
        super(props)
        this.state={
          form:{
            name:null,
            description:null,
          },
          errors:{}
        }
      }
      valideteform=()=>{
        const {form} = this.state 
        let errorsform ={} 
        let formsIsValid = true
        if(!form.name){
          formsIsValid = false
          errorsform['name'] = "El nombre de la categoria no puede estar vacio"
        }
        const validateItIs= /^[a-zA-Z0-9\s\u00f1\u00d1]{6,}$/g
        if(!validateItIs.test(form.name)){
          formsIsValid= false
          errorsform['name']="el nombre de la categoria no puede tener signos especiales y tiene que ser mayor o igual a 6"
        }
        if(!form.description){
          formsIsValid = false
          errorsform['description'] = "la descripcion  no puede estar vacio"
        }
        this.setState({errors:errorsform})
        return formsIsValid
      }

    changeTextInput =(e)=>{ 
      const value = e.target.value 
      const property = e.target.name 
    
      this.setState(prevState=>(
        {form:{
            ...prevState.form,
            [property]:value,
          }
        }
      ))
    }
    createnewPublications=(e)=>{
        e.preventDefault()
        const cc=new categoryClass
        const {form} = this.state
        if(this.valideteform()){
            const mthis= this
            //esto nos sirvio para crear roles y asignar al usuario admin
            /*Meteor.call('createRoles',form,function(error,resp){
              if(error){
                  
                  alert(error.reason)
              }
              else{
                  alert(resp)
                  
              }
          })*/
           
          cc.callMethod('createNewCategory',form,(error,result)=>{
                if(error){
                  alert (error)
                }else{
                  alert (result)
                  document.getElementById("newCategory").reset()
                  mthis.setState({form:{
                    name:null,
                    description:null,
                  }
                })
                  
                }
      
              })
        } 
    }

render() {
    const {errors}=this.state 
return (
        <div>
           <div className="card">
               <form  onSubmit={this.createnewPublications}id="newCategory">
                    <div className="card-header">
                        <h4>Crear Categoria</h4>
                    </div>
                    <div className="card-body">
                        <div className="form-row">
                            <div className="form-group col-md-6">
                                <label htmlFor="name">Nombre de Categoria</label>
                                <input type="text" className={errors.description?"form-control is-invalid":"form-control" }id="name" name={'name'} placeholder="categoria" autoComplete="off" onChange={this.changeTextInput}/>
                                {errors.name?<div className="invalid-feedback">{errors.name}</div>:null}
                            </div>
                            <div className="form-group col-md-6">
                                <label htmlFor="description">Descripcion</label>
                                <input type="text" className={errors.description?"form-control is-invalid":"form-control " } id="description" name={'description'} placeholder="Descripcion" autoComplete="off" onChange={this.changeTextInput}/>
                                {errors.description?<div className="invalid-feedback">{errors.description}</div>:null}
                            </div>
                        </div>
                    </div>
                    <div className="card-footer">
                        <button type="submit" className="btn btn-primary">Crear categoria</button>
                    </div>
               </form>
              
           </div>

        </div>
        )
    }
}