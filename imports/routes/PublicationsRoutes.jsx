import CreatePublication from '/imports/view/publicaciones/CreatePublication'
import UserPublications from '/imports/view/publicaciones/userPublications'
import EditPublication from '../view/publicaciones/EditPublication'

export default routes =[
    {
        path:"/dashboard/create-publications",
        component:CreatePublication,
        permission:"createpublication"
    },
    {
        path:"/dashboard/edit-publications",
        component:EditPublication,
        permission:"editpublication"
    },
    {
        path:"/dashboard/user-publications",
        component:UserPublications,
        permission:"allpublicationuser"
    },

]